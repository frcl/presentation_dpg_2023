import numpy as np
from manim import *


class SelfEnergyCorrection(Scene):
    def construct(self):
        # phi-u-loop
        leg1 = Line(start=2*LEFT, end=LEFT)
        loop1 = Arc(start_angle=PI, angle=-PI)
        loop2 = DashedVMobject(Arc(start_angle=PI, angle=PI), num_dashes=8)
        leg2 = Line(start=RIGHT, end=2*RIGHT)
        self.play(Create(leg1))
        self.play(Create(loop1), Create(loop2))
        self.play(Create(leg2))
        g1 = VGroup(leg1, loop1, loop2, leg2)
        g1.generate_target()
        g1.target.shift(3*LEFT + 2*UP).scale(0.5)
        self.play(MoveToTarget(g1))

        # phi-loop
        leg1 = Line(start=2*LEFT, end=ORIGIN)
        loop = Arc(arc_center=UP, start_angle=-PI/2, angle=-2*PI)
        leg2 = Line(start=ORIGIN, end=2*RIGHT)
        self.play(Create(leg1))
        self.play(Create(loop))
        self.play(Create(leg2))
        g2 = VGroup(leg1, loop, leg2)
        g2.generate_target()
        g2.target.shift(UP).scale(0.5)
        self.play(MoveToTarget(g2))

        # phi-phi-loop
        leg1 = DashedVMobject(Line(start=2*LEFT, end=LEFT), num_dashes=5)
        loop1 = Arc(start_angle=PI, angle=-PI)
        loop2 = Arc(start_angle=PI, angle=PI)
        leg2 = DashedVMobject(Line(start=RIGHT, end=2*RIGHT), num_dashes=5)
        self.play(Create(leg1))
        self.play(Create(loop1), Create(loop2))
        self.play(Create(leg2))
        g3 = VGroup(leg1, loop1, loop2, leg2)
        g3.generate_target()
        g3.target.shift(3*RIGHT + 2*UP).scale(0.5)
        self.play(MoveToTarget(g3))


class AllDiagrams(Scene):
    def construct(self):

        ## Self energy

        # phi-u-loop
        leg1 = Line(start=2*LEFT, end=LEFT)
        loop1 = Arc(start_angle=PI, angle=-PI)
        loop2 = DashedVMobject(Arc(start_angle=PI, angle=PI), num_dashes=8)
        leg2 = Line(start=RIGHT, end=2*RIGHT)
        self.play(Create(leg1))
        self.play(Create(loop1), Create(loop2))
        self.play(Create(leg2))
        g1 = VGroup(leg1, loop1, loop2, leg2)
        g1.generate_target()
        g1.target.shift(3*LEFT + 2*UP).scale(0.5)
        self.play(MoveToTarget(g1))

        # phi-loop
        leg1 = Line(start=2*LEFT, end=ORIGIN)
        loop = Arc(arc_center=UP, start_angle=-PI/2, angle=-2*PI)
        leg2 = Line(start=ORIGIN, end=2*RIGHT)
        self.play(Create(leg1))
        self.play(Create(loop))
        self.play(Create(leg2))
        g2 = VGroup(leg1, loop, leg2)
        g2.generate_target()
        g2.target.shift(UP).scale(0.5)
        self.play(MoveToTarget(g2))

        # phi-phi-loop
        leg1 = DashedVMobject(Line(start=2*LEFT, end=LEFT), num_dashes=5)
        loop1 = Arc(start_angle=PI, angle=-PI)
        loop2 = Arc(start_angle=PI, angle=PI)
        leg2 = DashedVMobject(Line(start=RIGHT, end=2*RIGHT), num_dashes=5)
        self.play(Create(leg1))
        self.play(Create(loop1), Create(loop2))
        self.play(Create(leg2))
        g3 = VGroup(leg1, loop1, loop2, leg2)
        g3.generate_target()
        g3.target.shift(3*RIGHT + 2*UP).scale(0.5)
        self.play(MoveToTarget(g3))

        ## Vertex

        # phi-phi to phi-phi
        leg1 = Line(start=2*LEFT+UP, end=LEFT)
        leg2 = Line(start=2*LEFT+DOWN, end=LEFT)
        loop1 = Arc(start_angle=PI, angle=-PI)
        loop2 = Arc(start_angle=PI, angle=PI)
        leg3 = Line(start=RIGHT, end=2*RIGHT+UP)
        leg4 = Line(start=RIGHT, end=2*RIGHT+DOWN)
        self.play(Create(leg1), Create(leg2))
        self.play(Create(loop1), Create(loop2))
        self.play(Create(leg3), Create(leg4))
        g4 = VGroup(leg1, leg2, loop1, loop2, leg3, leg4)
        g4.generate_target()
        g4.target.shift(3*LEFT + 2*DOWN).scale(0.5)
        self.play(MoveToTarget(g4))

        leg1 = Line(start=1.5*LEFT+UP, end=0.5*LEFT)
        leg2 = Line(start=1.5*LEFT+DOWN, end=0.5*LEFT)
        loop1 = Line(start=0.5*LEFT, end=0.5*RIGHT+UP)
        loop2 = Line(start=0.5*LEFT, end=0.5*RIGHT+DOWN)
        loop3 = DashedVMobject(Line(start=0.5*RIGHT+UP, end=0.5*RIGHT+DOWN), num_dashes=5)
        leg3 = Line(start=0.5*RIGHT+UP, end=1.5*RIGHT+UP)
        leg4 = Line(start=0.5*RIGHT+DOWN, end=1.5*RIGHT+DOWN)
        self.play(Create(leg1), Create(leg2))
        self.play(Create(loop1), Create(loop2), Create(loop3))
        self.play(Create(leg3), Create(leg4))
        g4 = VGroup(leg1, leg2, loop1, loop2, loop3, leg3, leg4)
        g4.generate_target()
        g4.target.shift(2*DOWN).scale(0.5)
        self.play(MoveToTarget(g4))

        leg1 = Line(start=2*LEFT+UP, end=LEFT+UP)
        leg2 = Line(start=2*LEFT+DOWN, end=LEFT+DOWN)
        loop1 = Line(start=LEFT+UP, end=RIGHT+UP)
        loop2 = Line(start=LEFT+DOWN, end=RIGHT+DOWN)
        loop3 = DashedVMobject(Line(start=RIGHT+UP, end=RIGHT+DOWN), num_dashes=5)
        loop4 = DashedVMobject(Line(start=LEFT+DOWN, end=LEFT+UP), num_dashes=5)
        leg3 = Line(start=RIGHT+UP, end=2*RIGHT+UP)
        leg4 = Line(start=RIGHT+DOWN, end=2*RIGHT+DOWN)
        self.play(Create(leg1), Create(leg2))
        self.play(Create(loop1), Create(loop2), Create(loop3), Create(loop4))
        self.play(Create(leg3), Create(leg4))
        g4 = VGroup(leg1, leg2, loop1, loop2, loop3, loop4, leg3, leg4)
        g4.generate_target()
        g4.target.shift(2*DOWN+3*RIGHT).scale(0.5)
        self.play(MoveToTarget(g4))

        # u to phi-phi
        leg1 = DashedVMobject(Line(start=2*LEFT, end=LEFT), num_dashes=5)
        loop1 = Arc(start_angle=PI, angle=-PI)
        loop2 = Arc(start_angle=PI, angle=PI)
        leg3 = Line(start=RIGHT, end=2*RIGHT+UP)
        leg4 = Line(start=RIGHT, end=2*RIGHT+DOWN)
        self.play(Create(leg1))
        self.play(Create(loop1), Create(loop2))
        self.play(Create(leg3), Create(leg4))
        g4 = VGroup(leg1, loop1, loop2, leg3, leg4)
        g4.generate_target()
        g4.target.shift(3*LEFT).scale(0.5)
        self.play(MoveToTarget(g4))

        leg1 = DashedVMobject(Line(start=1.5*LEFT, end=0.5*LEFT), num_dashes=5)
        loop1 = Line(start=0.5*LEFT, end=0.5*RIGHT+UP)
        loop2 = Line(start=0.5*LEFT, end=0.5*RIGHT+DOWN)
        loop3 = DashedVMobject(Line(start=0.5*RIGHT+UP, end=0.5*RIGHT+DOWN), num_dashes=5)
        leg3 = Line(start=0.5*RIGHT+UP, end=1.5*RIGHT+UP)
        leg4 = Line(start=0.5*RIGHT+DOWN, end=1.5*RIGHT+DOWN)
        self.play(Create(leg1))
        self.play(Create(loop1), Create(loop2), Create(loop3))
        self.play(Create(leg3), Create(leg4))
        g4 = VGroup(leg1, loop1, loop2, loop3, leg3, leg4)
        g4.generate_target()
        g4.target.shift(3*RIGHT).scale(0.5)
        self.play(MoveToTarget(g4))
