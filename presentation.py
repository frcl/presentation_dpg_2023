from kitmanim import KITScene
from manim import *
from manim_editor import PresentationSectionType


COLOR_TEMPLATE = TexTemplate()
COLOR_TEMPLATE.add_to_preamble(r"\usepackage{color}")
TIKZ_TEMPLATE = TexTemplate()
TIKZ_TEMPLATE.add_to_preamble(r"\usepackage{tikz}")


FONT_SIZE=30


class DPG23Pres(KITScene):
    title = r"Quantum Criticality on a Compressible Lattice"
    # subtitle = "demonstrating usage ..."
    presenters = ["Lars Franke"]
    coauthors = ["Saheli Sakar", "Niko Grivas", "Markus Garst"]
    date = "30.04.2023"
    institute = "KIT - TFP"

    def construct(self):
        self.title_slide(animation=FadeIn, title_clean="remove")

        with self.slide("Criticallity on a compressible lattice", animation=FadeIn):
            self.slide_intro_animation_rigid()

        with self.slide("Criticallity on a compressible lattice", animation=None):
            self.slide_intro_animation_on_lattice()

        classic_title = r"""\begin{flushleft}Historical background:\\
        Classical criticallity on a compressible lattice\end{flushleft}"""

        with self.slide(classic_title, animation=FadeIn):
            self.slide_classic_intro()

        with self.slide(classic_title, animation=None):
            self.slide_larkin_pikin()

        with self.slide(classic_title, animation=None):
            self.slide_bergman_halperin()

        quantum_title_template = r"""\begin{{flushleft}}Quantum criticality on a compressible lattice\\
        {}\end{{flushleft}}"""

        with self.slide("Quantum criticality on a compressible lattice", animation=FadeIn):
            self.slide_quantum_critical_model()

        with self.slide(quantum_title_template.format("Procedure and challenges"), animation=FadeIn):
            self.slide_rg()

        with self.slide(quantum_title_template.format("Structural instability"), animation=FadeIn):
            self.slide_structural_instability()

        with self.slide(quantum_title_template.format("RG flow"), animation=FadeIn):
            self.slide_flow()

        with self.slide(quantum_title_template.format("Experimental relevance"), animation=FadeIn):
            self.slide_experiment()

        with self.slide(r"Conclusion", animation=FadeIn):
            self.slide_conclusion()

        # TODO: sort out overlap/contradiction of slide titles and footer

    def slide_intro_animation_rigid(self):
        var_time = Variable(0., 't', num_decimal_places=2)
        var_amp = Variable(0., 't', num_decimal_places=2)
        time = var_time.tracker
        amplitude = var_amp.tracker

        center = np.zeros(3)
        fluctuating_lattice = VGroup()
        for x in np.arange(-3.5, 3.6, 1):
            for y in np.arange(-2.0, 2.1, 1):
                fluctuating_lattice += Arrow(start=center+np.array([x, y-0.5, 0]),
                                             end=center+np.array([x, y+0.5, 0]),
                                             color=BLACK) \
                    .add_updater(lambda m, x=x, y=y: m.set_angle(
                            # m.get_angle()
                            # +np.pi/2+2*np.pi*amplitude.get_value()*np.sum(wave(x, y, 0, time.get_value())),
                            +np.pi/2+2*np.pi*amplitude.get_value()*np.sum(wave(x, y, 0, time.get_value())),
                            about_point=center+np.array([x, y, 0]),
                    ))

        self.play(Create(fluctuating_lattice))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(time.animate(run_time=5).set_value(20), amplitude.animate(run_time=1).set_value(0.2))
        self.wait()

    def slide_intro_animation_on_lattice(self):
        var_time = Variable(0., 't', num_decimal_places=2)
        var_amp = Variable(0., 't', num_decimal_places=2)
        time = var_time.tracker
        amplitude = var_amp.tracker

        center = np.zeros(3)
        fluctuating_vectorfield = VGroup()
        for x in np.arange(-3.5, 3.6, 1):
            for y in np.arange(-2.0, 2.1, 1):
                fluctuating_vectorfield += Arrow(start=center+np.array([x, y-0.5, 0]),
                                                 end=center+np.array([x, y+0.5, 0]),
                                                 color=BLACK) \
                    .add_updater(lambda m, x=x, y=y: m.set_angle(
                            # m.get_angle()
                            +np.pi/2+2*np.pi*5*amplitude.get_value()*np.sum(wave(x, y, 0, time.get_value())),
                            about_point=center+np.array([x, y, 0]),
                    ))

        var_time = Variable(0., 't', num_decimal_places=2)
        var_amp = Variable(0., 't', num_decimal_places=2)
        time = var_time.tracker
        amplitude = var_amp.tracker

        fluctuating_lattice = VGroup()
        dots = []
        for x in np.arange(-3.5, 3.6, 1):
            dots_line = []
            for y in np.arange(-2.0, 2.1, 1):
                dot = Dot(center+np.array([x, y, 0]), color=BLACK) \
                    .add_updater(lambda m, x=x, y=y: m.set_y(
                            center[1]+y+amplitude.get_value()*wave(x, 0, 0, time.get_value())[1]
                        ).set_x(
                            center[0]+x+amplitude.get_value()*wave(0, y, 0, time.get_value())[0]
                    ))
                fluctuating_lattice += dot
                dots_line.append(dot)
            dots.append(dots_line)

        def line_updater(dot1, dot2):
            def update_line(m, x=x, y=y):
                m.put_start_and_end_on(dot1.get_center(), dot2.get_center())
                return m
            return update_line

        fluctuating_lattice_lines = VGroup()
        for i in range(len(dots)):
            dots_line = dots[i]
            for j in range(len(dots_line)):
                if not i+1 == len(dots):
                    dot1, dot2 = dots_line[j], dots[i+1][j]
                    fluctuating_lattice_lines += Line(dot1, dot2, color=BLACK, stroke_width=1) \
                        .add_updater(line_updater(dot1, dot2))
                if not j+1 == len(dots_line):
                    dot1, dot2 = dots_line[j], dots_line[j+1]
                    fluctuating_lattice_lines += Line(dot1, dot2, color=BLACK, stroke_width=1) \
                        .add_updater(line_updater(dot1, dot2))

        fluctuating_vectorfield = VGroup()
        for x in np.arange(-3.5, 3.6, 1):
            for y in np.arange(-2.0, 2.1, 1):
                fluctuating_vectorfield += Arrow(start=center+np.array([x, y-0.5, 0]),
                                             end=center+np.array([x, y+0.5, 0]),
                                             color=BLACK) \
                    .add_updater(lambda m, x=x, y=y: m.move_to([
                            center[0]+x+amplitude.get_value()*wave(0, y, 0, time.get_value())[0],
                            center[1]+y+amplitude.get_value()*wave(x, 0, 0, time.get_value())[1],
                            0
                        ]).set_angle(
                            # m.get_angle()
                            +np.pi/2+3*2*np.pi*amplitude.get_value()*np.sum(wave(x, y, 0, time.get_value())),
                            about_point=m.get_center(),
                    ))

        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(
            Create(fluctuating_lattice),
            Create(fluctuating_lattice_lines),
            Create(fluctuating_vectorfield),
        )
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(time.animate(run_time=5).set_value(20), amplitude.animate(run_time=1).set_value(0.1))
        self.wait()

    def slide_classic_intro(self):
        phi4_action = mathtex(r'S = \int d^dr \left(\frac12 \phi^T (-\nabla^2 + r) \phi + u (\phi\cdot\phi)^4\right)')
        items = BulletedList(
            r"$O(N)$ symmetric $\phi^4$-theory as a model of a second order transition",
            phi4_action,
            r"Quadratic coupling to strain",
            mathtex(r'\mathcal{L}_\mathrm{int} =  \lambda\; \mathrm{tr}\{\varepsilon\}\; \phi^2'),
            r"where",
            mathtex(r'\varepsilon_{ij}(\mathbf{r}) = E_{ij} + \frac12 \left( \partial_i u_j(\mathbf{r}) + \partial_j u_i(\mathbf{r})\right)'),
            color=BLACK, font_size=FONT_SIZE, buff=0.3,
        ).to_edge(LEFT)

        phi_def = MathTex(
            r"\vec{\phi} = (\phi_1, \dots, \phi_N)",
            color=BLACK, font_size=22,
        ).next_to(phi4_action, RIGHT, buff=2)
        for item in items:
            self.play(FadeIn(item))
            if item == phi4_action:
                self.play(FadeIn(phi_def))
            self.next_section(type=PresentationSectionType.NORMAL)

        center = 1*RIGHT + 1*DOWN

        var_e_xx = Variable(0., r'\varepsilon_{xx}', num_decimal_places=2)
        var_e_yy = Variable(0., r'\varepsilon_{yy}', num_decimal_places=2)
        # e_vars = VGroup(var_e_xx, var_e_yy).arrange(DOWN).scale(0.6).move_to(center+3.5*RIGHT)

        e_xx = var_e_xx.tracker
        e_yy = var_e_yy.tracker
        distorted_lattice = VGroup()
        for x in np.arange(-0.75, 0.76, 0.5):
            for y in np.arange(-0.75, 0.76, 0.5):
                # e_xx and e_yy
                distorted_lattice += Dot(center+np.array([x, y, 0]), color=BLACK, radius=0.05) \
                    .add_updater(lambda m, x=x: m.set_x(center[0]+x*(1+e_xx.get_value()))) \
                    .add_updater(lambda m, y=y: m.set_y(center[1]+y*(1+e_yy.get_value())))

        larrow = Arrow(start=[0, 0, 0], end=RIGHT, color=BLACK).move_to(center+1.5*LEFT)
        rarrow = Arrow(start=[0, 0, 0], end=LEFT, color=BLACK).move_to(center+1.5*RIGHT)

        self.next_section(type=PresentationSectionType.NORMAL)

        self.play(Create(distorted_lattice)) #, Create(e_vars))
        self.wait()
        self.next_section(type=PresentationSectionType.NORMAL)

        self.play(Create(larrow), Create(rarrow))
        self.play(e_xx.animate.set_value(-0.2),
                  e_yy.animate.set_value(0.2))
        self.wait()

        # shear strain
        center_shear = 5*RIGHT + DOWN

        var_e_xy = Variable(0., r'\varepsilon_{xy}', num_decimal_places=2)
        # var_e_xy.scale(0.6).move_to(center+3*RIGHT)

        e_xy = var_e_xy.tracker
        distorted_lattice = VGroup()
        for x in np.arange(-0.75, 0.76, 0.5):
            for y in np.arange(-0.75, 0.76, 0.5):
                # e_xy
                distorted_lattice += Dot(center_shear+np.array([x, y, 0]), color=BLACK, radius=0.05) \
                    .add_updater(lambda m, x=x, y=y: m.set_y(center_shear[1]+y+x*e_xy.get_value()))

        larrow = Arrow(start=0.5*DOWN, end=0.5*UP, color=BLACK).move_to(center_shear+1.5*LEFT)
        rarrow = Arrow(start=0.5*UP, end=0.5*DOWN, color=BLACK).move_to(center_shear+1.5*RIGHT)

        self.next_section(type=PresentationSectionType.NORMAL)

        self.play(Create(distorted_lattice)) #, Create(var_e_xy))
        self.wait()

        self.next_section(type=PresentationSectionType.NORMAL)

        self.play(Create(larrow), Create(rarrow))
        self.play(e_xy.animate.set_value(-0.2))
        self.wait()

    def slide_larkin_pikin(self):

        quadratic_coupling = mathtex(r'\mathcal{L}_\mathrm{int} = \lambda\;\mathrm{tr}\{\varepsilon\}\;\phi^2')
        path_integral = mathtex(r'Z = \int \mathcal{D}\phi\mathcal{D}\mathbf{u}\;e^{- \int d^dr \mathcal{L}}')
        bulk_modulus_corr = mathtex(
            r'\Delta K \propto \frac{\partial^2 F}{\partial \varepsilon^2}'
            r'\propto \langle \phi^2 \phi^2 \rangle \propto \xi^{-\alpha}'
        )
        bulk_modulus = mathtex(r"K = K_0 - \Delta K")
        scaling_relation = MathTex(r"\alpha = 2 - \nu d", font_size=22, color=BLACK)

        items = BulletedList(
            r"""\textbf{Larkin-Pikin criterion}: If $\alpha > 0$ \\
                at constant pressure a structural instability \\
                causes the transition to become first order""",
            r"Correction to bulk modulus",
            bulk_modulus,
            r"$\Rightarrow$ Instability if $\nu d < 2$",
        ).to_edge(LEFT)

        self.add_citation("[A. Larkin, S. Pikin, Sov. Phys. JETP 29, 891 (1969)]")

        for item in items:
            self.play(FadeIn(item))
            # self.next_section(type=PresentationSectionType.NORMAL)
            self.subsection("foo")
            if item == bulk_modulus:
                self.play(FadeIn(quadratic_coupling.move_to(3*RIGHT+1.5*UP)))
                self.play(FadeIn(path_integral.move_to(3*RIGHT+0.5*UP)))
                # self.next_section(type=PresentationSectionType.NORMAL)
                self.subsection("foo")
                self.play(FadeIn(bulk_modulus_corr.move_to(3*RIGHT+0.5*DOWN)))
                # self.next_section(type=PresentationSectionType.NORMAL)
                self.subsection("foo")
                self.play(FadeIn(scaling_relation.move_to(4*RIGHT+1.1*DOWN)))
                # self.next_section(type=PresentationSectionType.NORMAL)
                self.subsection("foo")

    def slide_bergman_halperin(self):
        small_action = mathtex(r'S = \int', r'd^dr', r'\Bigg(&',
            r'\frac12 \phi^T (-\nabla^2 + r) \phi'
            r'+ u (\phi\cdot\phi)^2',
            r'\Bigg)')
        big_action = mathtex(r'S = \int', r'd^dr', r'\Bigg(&',
            r'\frac12 \phi^T (-\nabla^2 + r) \phi'
            r'+ u (\phi\cdot\phi)^2', r'\\',
            r'&+ \lambda\, \mathrm{tr}\{\varepsilon\}\, \phi^2',
            r'+ \frac12 \partial_i u_j C_{ijkl} \partial_k u_l',
            r'\Bigg)')

        displayed_action = big_action.copy()

        items = BulletedList(
            r"Renormalization group analysis",
            displayed_action,
            r"New stable fixed point\\ with Fisher-renormalized exponents\\ {} [M. E. Fisher, Phys. Rev. 176, 257 (1968)]",
            mathtex(r"\alpha_\mathrm{eff} = - \frac{\alpha}{1-\alpha} \qquad \beta_\mathrm{eff} = \frac{\beta}{1-\alpha} \qquad \dots"),
            r"To stabilize criticality surface atoms \\ need to be fixed (''clamped boundary conditions'')",
            buff=0.1,
        ).to_edge(LEFT)

        small_action.move_to(displayed_action)
        big_action.move_to(displayed_action)
        displayed_action.become(small_action)

        self.add_citation("[D. J. Bergman, B. I. Halperin, Phys. Rev. B 13, 2145 (1976)]")

        plot = ImageMobject(
            '/home/lars/phd/presentation_dpg_2023spring/figures/bergmanhalperin_flow.png',
        ).scale(1).move_to(3.8*RIGHT + 0.5*UP)
        # TODO: define lambda tilde

        for item in items:
            self.play(FadeIn(item))
            self.next_section(type=PresentationSectionType.NORMAL)
            if item == displayed_action:
                self.play(displayed_action.animate.become(big_action))
                self.next_section(type=PresentationSectionType.NORMAL)
                self.play(FadeIn(plot))
                self.next_section(type=PresentationSectionType.NORMAL)

    def slide_quantum_critical_model(self):
        figure = ImageMobject(
            '/home/lars/phd/presentation_dpg_2023spring/figures/QuantumPhaseTransition.png',
        ).scale(0.8)
        # TODO: "citation" https://commons.wikimedia.org/wiki/File:QuantumPhaseTransition.png
        self.play(FadeIn(figure))
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(figure.animate.scale(0.5).move_to(RIGHT*4.5 + UP*1.5))

        items = BulletedList(
            r"Quantum critical model in imaginary time formalism",
            MathTex(r"""
                S = \int d^dr {\color{red} d\tau} \Bigg(&
                    \frac12 \phi^T ({\color{red} -\partial_\tau^2} - c^2\nabla^2 + r) \phi
                    + u (\phi\cdot\phi)^2 \\
                    &+ \lambda \mathrm{tr}\{\varepsilon\} \phi^2
                    + \frac12 u_i ({\color{red} -\delta_{il} \rho \partial_\tau^2}
                        - C_{ijkl} \partial_j \partial_k) u_l
                \Bigg)""", color=BLACK, font_size=FONT_SIZE, tex_template=COLOR_TEMPLATE),
            r"Expectation: Similar criterion to Larkin-Pikin with $d \to d + z$",
            mathtex(r"\nu (d+z) < 2"),
            r"where $z$ is the dynamical exponent $\omega \propto k^z$",
        ).to_edge(LEFT)

        for item in items:
            self.play(FadeIn(item))
            self.next_section(type=PresentationSectionType.NORMAL)

    def slide_rg(self):
        self_energy_integral = mathtex(
            r"""\propto \int_{\omega,q} \frac{1}{\big((\omega' - \omega)^2 + c^2 (q' - q)^2 + r\big)}
                \frac{q^2}{(\omega^2 + c_s^2 q^2)} \qquad\qquad c_s^2 = \frac{C_{11}}{\rho}"""
        )

        feynman_diagrams = ImageMobject(
            '/home/lars/phd/presentation_dpg_2023spring/figures/feynman_diagrams.png'
        ).scale(1.4).move_to(1.5*UP)

        self.play(FadeIn(feynman_diagrams))

        items = BulletedList(
            r"Renormalization group analysis to one-loop order",
            r"Critical and elastic fluctuations propagate at different speeds",
            self_energy_integral,
            r"Wave function renormalization ($Z$-Factor)",
        ).to_edge(LEFT).shift(DOWN)

        for item in items:
            if item == self_energy_integral:
                item = item.shift(2*RIGHT)

                # phi-u-loop
                leg1 = Line(start=2*LEFT, end=LEFT, color=BLACK, stroke_width=2)
                loop1 = Arc(start_angle=PI, angle=-PI, color=BLACK, stroke_width=2)
                loop2 = DashedVMobject(Arc(start_angle=PI, angle=PI, color=BLACK, stroke_width=2), num_dashes=8)
                leg2 = Line(start=RIGHT, end=2*RIGHT, color=BLACK, stroke_width=2)
                loop = VGroup(leg1, loop1, loop2, leg2).scale(0.4)
                loop.next_to(item, LEFT, 0.2)

                self.play(Create(leg1))
                self.play(Create(loop1), Create(loop2))
                self.play(Create(leg2))

            self.play(FadeIn(item))
            self.next_section(type=PresentationSectionType.NORMAL)

    def slide_structural_instability(self):
        flow_eqs = mathtex(r"\frac{dc^2}{d\ell} = - \frac{4}{3\pi^2\rho} \frac{\lambda^2c}{c_s(c+c_s)^3} \qquad \frac{dc_s^2}{d\ell} = - \frac{N}{4\pi^2\rho} \frac{\lambda^2}{c^3}")
        items = BulletedList(
            r"Both speeds flow with RG",
            flow_eqs,
            r"$c_s$ flowing to zero causes instability",
            mathtex(r"c_s^2 = \frac1\rho\left(K + \left(2-\frac{2}{d}\right)\mu\right)"),
        ).to_edge(LEFT)

        plot = ImageMobject(
            '/home/lars/phd/presentation_dpg_2023spring/figures/instability_example.png',
        ).scale(0.8).move_to(4*RIGHT)

        for item in items:
            self.play(FadeIn(item))
            self.next_section(type=PresentationSectionType.NORMAL)
            if item == flow_eqs:
                self.play(FadeIn(plot))
                self.next_section(type=PresentationSectionType.NORMAL)

    def slide_flow(self):
        alpha_def = mathtex(r'\alpha = \frac{4-N}{2(N+8)} \epsilon').move_to(5.5*RIGHT + 2.5*UP)

        nlt4_label = mathtex(r'N < 4')
        plot_nlt4 = ImageMobject(
            '/home/lars/phd/presentation_dpg_2023spring/figures/qccl_flow_N3.png',
        ).scale(1).move_to(3.5*LEFT + 0.5*DOWN)
        nlt4_label.next_to(plot_nlt4, UP, 0.1)

        ngt4_label = mathtex(r'N > 4')
        plot_ngt4 = ImageMobject(
            '/home/lars/phd/presentation_dpg_2023spring/figures/qccl_flow_N5.png',
        ).scale(1).move_to(3*RIGHT + 0.5*DOWN)
        ngt4_label.next_to(plot_ngt4, UP, 0.1)

        self.play(FadeIn(plot_nlt4), FadeIn(nlt4_label))
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(FadeIn(plot_ngt4), FadeIn(ngt4_label))
        self.next_section(type=PresentationSectionType.NORMAL)
        self.play(FadeIn(alpha_def))
        self.next_section(type=PresentationSectionType.NORMAL)
        # TODO: define lambda tilde and u tilde

        self.play(FadeOut(plot_nlt4), FadeOut(nlt4_label))

        items = BulletedList(
            r"""Close to new fixed point the dynamical\\
                exponent of the phonons is renormalized\\
                by the flowing speed of sound""",
            mathtex(r"\omega = c_s(k) k^z"),
            mathtex(r"c_s \propto b^{\Delta z}"),
            mathtex(r"\Delta z = \frac12 \frac{N-4}{N+8} \epsilon"),
        ).to_edge(LEFT)

        for item in items:
            self.play(FadeIn(item))
            self.next_section(type=PresentationSectionType.NORMAL)

    def slide_experiment(self):
        items = BulletedList(
            r"Antiferromagnetic QPT in\\ pressure tuned $\mathrm{TlCuCl}_3$",
        ).to_edge(LEFT).shift(2*UP)

        for item in items:
            self.play(FadeIn(item))
            self.next_section(type=PresentationSectionType.NORMAL)

        figure = ImageMobject(
            '/home/lars/phd/presentation_dpg_2023spring/figures/TlCuCl3.png',
        ).scale(1.6).move_to(2*RIGHT + 0.5*UP)
        self.play(FadeIn(figure))
        self.add_citation("[Ch. Rüegg et al., Phys. Rev. Lett. 100, 205701 (2008)]")

    def slide_conclusion(self):
        items = BulletedList(
            r"The quantum version of the Larkin-Pikin criterion is valid",
            mathtex(r"\nu (d+z) < 2"),
            r"""Even when the criterion is not satisfied the instability\\
                can still occur depending on system parameters\\
                (separatrix in parameter space)""",
            r"""Renormalized dynamical exponent of the phonon\\
                (if bare couplings are close to the separatrix)""",
            mathtex(r"\omega = c_s(k) k^z"),
        ).to_edge(LEFT).shift(UP)

        # TODO: small pictures of flow diagram with z-renormalized fixed point

        for item in items:
            self.play(FadeIn(item))
            self.next_section(type=PresentationSectionType.NORMAL)

        people = [
            (ImageMobject('/home/lars/phd/presentation_dpg_2023spring/people/SaheliSarkar_rdax_230x288.jpg'
                          ).scale_to_fit_width(1),
             Text('Saheli Sarkar', color=BLACK, font_size=16)),
            (ImageMobject('/home/lars/phd/presentation_dpg_2023spring/people/NikoGrivas90.jpg'
                          ).scale_to_fit_width(1),
             Text('Niko Grivas', color=BLACK, font_size=16)),
            (ImageMobject('/home/lars/phd/presentation_dpg_2023spring/people/MarkusGarst.jpg'
                          ).scale_to_fit_width(1),
             Text('Markus Garst', color=BLACK, font_size=16)),
        ]
        people = Group(
            *[Group(*pair).arrange(DOWN) for pair in people]
        ).arrange(LEFT).to_edge(DOWN).to_edge(RIGHT)
        self.play(FadeIn(people))


    def add_citation(self, citation_text):
        citation = self.text(citation_text, font_size=20).to_edge(DOWN).to_edge(RIGHT)
        self.play(FadeIn(citation))


def wave(x, y, z, t):
    # TODO: make more random
    return (np.sin(y - 0.2*t)*np.sin(2*y - t), np.sin(x - 0.1*t)*np.sin(x - t), 0)


def mathtex(*tex_strings):
    return MathTex(*tex_strings, color=BLACK, font_size=FONT_SIZE)


class BulletedList(VGroup):
    def __init__(
        self,
        *items,
        buff=0.3,
        dot_scale_factor=3,
        dot_color=BLACK,
        font_size=FONT_SIZE,
        color=BLACK,
        tex_environment=None,
        **kwargs,
    ):
        self.buff = buff
        self.dot_scale_factor = dot_scale_factor
        self.tex_environment = tex_environment
        self.dot_color = dot_color

        super().__init__()

        for item in items:
            if isinstance(item, MathTex):
                if 'color' in kwargs:
                    item.set_color(kwargs['color'])
                self.add(item)
            else:
                tex_item = Tex(
                    item,
                    tex_environment=tex_environment,
                    font_size=font_size,
                    color=color,
                    **kwargs
                )
                self.add(tex_item)
                dot = MathTex("\\cdot", color=self.dot_color).scale(self.dot_scale_factor)
                dot.next_to(tex_item[0], LEFT, 0.3, aligned_edge=UP)
                tex_item.add_to_back(dot)

        self.arrange(DOWN, aligned_edge=LEFT, buff=self.buff)
        for item in self:
            if isinstance(item, MathTex) and not isinstance(item, Tex):
                item.shift(RIGHT)

    def fade_all_but(self, index_or_string, opacity=0.5):
        arg = index_or_string
        if isinstance(arg, str):
            part = self.get_part_by_tex(arg)
        elif isinstance(arg, int):
            part = self.submobjects[arg]
        else:
            raise TypeError(f"Expected int or string, got {arg}")
        for other_part in self.submobjects:
            if other_part is part:
                other_part.set_fill(opacity=1)
            else:
                other_part.set_fill(opacity=opacity)


if __name__ == "__main__":
    with tempconfig(
        {
            "quality": "high_quality",
            "format": "gif",
            "renderer": "cairo",
            "save_section": True,
        }
    ):
        pres = Presentation()
        pres.render()
