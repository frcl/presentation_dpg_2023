common_options = -qp --save_sections --progress_bar none


slides: *_slides.py
	for pyfile in $^; do manim $(common_options) $$pyfile -a; done
