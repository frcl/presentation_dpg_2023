from manim import *


class Action(Scene):

    def construct(self):
        action = MathTex(
            r'S = \int d\tau d^dx \Big[',
            r'\frac12 \phi^T(\tau, x) ( - \partial_\tau^2 - c^2\nabla^2 + r) \phi(\tau, x)',
            r'\\+',
            r'\frac{\gamma}{4!} (\phi(\tau, x)\cdot\phi(\tau, x))^2',
            r'\\+',
            r'\frac12 u_i(\tau, x) (- \rho\partial_\tau^2 \delta_{il} - C_{ijkl} \partial_j \partial_k) u_l(\tau, x)',
            r'\\+',
            r'\lambda (\phi(\tau, x) \cdot \phi(\tau, x)) (\nabla \cdot u(\tau, x))',
            r'\Big]',
        )
        self.play(Write(action))
        self.wait()


class Scaling(Scene):

    def construct(self):
        ax1 = NumberPlane(y_range=[-10, 10]).scale(2)
        ax2 = ax1.copy()
        self.play(Create(ax1))
        self.wait()
        rt = 3 # s
        self.play(
            FadeOut(ax1, scale=0.3, run_time=rt),
            FadeIn(ax2, scale=3.0, run_time=rt)
        )
        self.play(
            FadeOut(ax2, scale=0.3, run_time=rt),
            FadeIn(ax1, scale=3.0, run_time=rt)
        )
        self.play(
            FadeOut(ax1, scale=0.3, run_time=rt),
            FadeIn(ax2, scale=3.0, run_time=rt)
        )
        self.wait()
